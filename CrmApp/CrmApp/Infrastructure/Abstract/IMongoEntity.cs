﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmApp.Infrastructure.Abstract
{
    public interface IMongoEntity
    {
        ObjectId Id { get; set; }
    }
}
