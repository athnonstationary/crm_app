﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmApp.Infrastructure.Abstract
{
    public interface IMongoRepository<T> where T:IMongoEntity
    {
        Task<List<T>> GetAll(string filter = "");
        Task<T> GetSingle(ObjectId id);
        Task Insert(T customer);
        Task<bool> Update(string id, T updateValue);
        Task<bool> Delete(string id);
    }
}
