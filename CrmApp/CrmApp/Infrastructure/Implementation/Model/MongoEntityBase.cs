﻿using CrmApp.Infrastructure.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace CrmApp.Infrastructure.Implementation.Model
{
    public abstract class MongoEntityBase : IMongoEntity
    {
        public ObjectId Id { get; set; }
    }
}
