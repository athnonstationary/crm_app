﻿using CrmApp.Infrastructure.Abstract;
using CrmApp.Infrastructure.Implementation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CrmApp.Infrastructure.Implementation
{
    public abstract class MongoRepositoryBase : IMongoRepository<MongoEntityBase>
    {
        protected readonly IMongoCollection<MongoEntityBase> _collection;

        public async Task<bool> Delete(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MongoEntityBase>> GetAll(string filter = "")
        {
            throw new NotImplementedException();
        }

        public async Task<MongoEntityBase> GetSingle(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public async Task Insert(MongoEntityBase customer)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Update(string id, MongoEntityBase updateValue)
        {
            throw new NotImplementedException();
        }
    }
}
