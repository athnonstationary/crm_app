﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRMapp.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace CRMapp.Infrastructure
{
    public class MongoRepository
    {
        private readonly IMongoCollection<Customer> _collection;

        public MongoRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("Crm");
            _collection = database.GetCollection<Customer>("Customers");
        }

        public async Task<List<Customer>> GetAll(string filter = "")
        {
            if (string.IsNullOrEmpty(filter))
            {
                return await _collection.AsQueryable().ToListAsync();
            }
            var query = _collection.AsQueryable().Where(x => x.FirstName.Contains(filter)
                                              || x.LastName.Contains(filter));

            return await query.ToListAsync();
        }
        public async Task<Customer> GetCustomer(ObjectId id)
        {
            var query = _collection.AsQueryable().SingleOrDefaultAsync(x => x.Id == id);
            return await query;
        }

        public async Task Insert(Customer customer)
        {
            await _collection.InsertOneAsync(customer);
        }

        public async Task<bool> UpdateCustomer(string id, Customer updateValue)
        {
            if (!ObjectId.TryParse(id, out var _id))
            {
                return false;
            }
            var result = await _collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == _id);

            if (result == null)
            {
                return false;
            }

            updateValue.Id = _id;

            await _collection.ReplaceOneAsync(doc => doc.Id == _id, updateValue);

            return true;
        }

        public async Task<bool> Delete(string id)
        {
            if (!ObjectId.TryParse(id, out var _id))
            {
                return false;
            }
            var result = await _collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == _id);

            if (result == null)
            {
                return false;
            }

            await _collection.DeleteOneAsync(x => x.Id == _id);

            return true;
        }
    }
}
