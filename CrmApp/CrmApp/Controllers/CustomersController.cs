﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRMapp.Infrastructure;
using CRMapp.Model;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace CRMapp.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {

        private readonly MongoRepository _customerRepository;

        public CustomersController()
        {
            _customerRepository = new MongoRepository();
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetCustomers(string filter)
        {
            return Ok(await _customerRepository.GetAll(filter));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomer(string id)
        {
            if (!ObjectId.TryParse(id, out var _id))
            {
                return BadRequest();
            }

            return Ok(await _customerRepository.GetCustomer(_id));
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post(Customer arg)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _customerRepository.Insert(arg);
            return Ok();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, Customer updateValue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _customerRepository.UpdateCustomer(id, updateValue);
            return result ? (IActionResult)Ok() : BadRequest();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _customerRepository.Delete(id);
            return result ? (IActionResult)Ok() : BadRequest();
        }
    }
}