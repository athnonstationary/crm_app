# Welcome

#Contents

1.   Docker
1.   MongoDB setup

#Installing docker and running [Docker](http://www.docker.com)
1. Install
1. Turn on virtualization

>     docker run -p 27017:27017 -d mongo

#Mongo setup

1. Connect via [Robo3T](http://www.robomongo.org)
1. Create CRM database and Customers document
1. Insert example data


```
#!json


db.getCollection('Customers').insert(
[{
	"FirstName": "Adam",
	"LastName": "Malinowski",
	"Roles": [{
		"Name": "Admin"
	}]
}, {
	"FirstName": "Jan",
	"LastName": "Kowalski",
	"Roles": [{
			"Name": "User"
		},
		{
			"Name": "Manager"
		}
	]
}]
)

```

*Test markdown lines
*Test markdown lines